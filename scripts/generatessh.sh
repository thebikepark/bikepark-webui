#!/bin/bash
echo $pwd
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout ./assets/certs/server.key \
    -new \
    -out ./assets/certs/server.crt \
    -config ./assets/openssl-custom.cnf \
    -sha256 \
    -days 365
