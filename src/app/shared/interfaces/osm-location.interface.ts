export interface OsmLocation {
  place_id: number
  licence: string
  osm_type: string
  osm_id: number
  boundingbox: string[]
  lat: string
  lon: string
  display_name: string
  class: string
  type: string
  importance: number
  address: OsmAddress
}

export interface OsmAddress {
  road: string
  neighbourhood: string
  suburb: string
  city: string
  county: string
  postcode: string
  country: string
  country_code: string
}
