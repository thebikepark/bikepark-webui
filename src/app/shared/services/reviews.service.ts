import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Review, ReviewsResponse } from '../interfaces/reviews.interface';
import { ApiServiceService } from './api-service.service';


export interface ReviewsFilter {
  "filter.place_id": string
  "user_id": string
}

@Injectable({
  providedIn: 'root'
})
export class ReviewsService extends ApiServiceService<Review, ReviewsResponse> {

  constructor(public http: HttpClient) {
    super(http)
    this.url = "reviews";
  }

}
