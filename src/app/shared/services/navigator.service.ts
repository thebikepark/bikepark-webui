import { Injectable } from '@angular/core';

export type LatLong = [number, number];

@Injectable({
  providedIn: 'root'
})
export class NavigatorService {

  constructor() { }

  currentPosition(): Promise<LatLong> {
    return new Promise((resolve, reject) => {
      if (!navigator.geolocation) {
        reject(new Error("Geolocation is not supported by your browser"));
      }

      navigator.geolocation.getCurrentPosition(
        (position) => {
          resolve([position.coords.latitude, position.coords.longitude])
        },
        (err) => {
          console.error(err)
          reject("Unable to retrieve your location")
        }
      )
    });
  }

}
