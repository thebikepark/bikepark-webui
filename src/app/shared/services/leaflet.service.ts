import { Injectable } from '@angular/core';
import { icon, Icon } from 'leaflet';


export const bikeParkingIconPath = 'assets/bike-parking.svg'
export const bikeTheftIconPath = 'assets/bike-theft.svg'

@Injectable({
  providedIn: 'root'
})
export class LeafletService {

  static URL = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
  // static URL = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png'
  // static URL = 'http://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png'

  public static icon(iconUrl: string): Icon {
    return icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: iconUrl,
      shadowUrl: 'leaflet/marker-shadow.png',
    })
  }

}
