import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapViewLocationComponent } from './map-view-location.component';

describe('MapViewLocationComponent', () => {
  let component: MapViewLocationComponent;
  let fixture: ComponentFixture<MapViewLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapViewLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapViewLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
