import { Component, Input, OnInit } from '@angular/core';
import { LatLng, latLng, Map, MapOptions, marker, Marker, MarkerOptions, tileLayer } from 'leaflet';
import { LocaitonType, Location } from '../../../modules/locations/interfaces/location.inteface';
import { bikeParkingIconPath, bikeTheftIconPath, LeafletService } from '../../services/leaflet.service';

@Component({
  selector: 'app-map-view-location',
  templateUrl: './map-view-location.component.html',
  styleUrls: ['./map-view-location.component.scss']
})
export class MapViewLocationComponent implements OnInit {

  options: any;
  map: Map;
  marker: Marker;

  @Input() location: Location;

  constructor() { }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12
    }
  }

  onMapReady(e: Map): void {
    this.map = e;
    const latLngE = latLng(this.location.latitude, this.location.longitude)
    this.marker = this.setMarker(latLngE)
    this.marker.addTo(this.map)
    setTimeout(() => {
      this.map.setView(latLngE, 16)
    }, 500)
  }

  private setMarker(latLng: LatLng): Marker {
    const m = this.createMarkerFromLocation(latLng)
    return m
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    var path = bikeParkingIconPath;
    if (this.location.type === LocaitonType.Theft) {
      path = bikeTheftIconPath;
    }
    const options: MarkerOptions = {
      draggable: false,
      icon: LeafletService.icon(path)
    }
    return marker(latLng, options)
  }

}
