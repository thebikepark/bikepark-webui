import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { LoginComponent } from './components/login/login.component';
import { MapViewLocationComponent } from './components/map-view-location/map-view-location.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SearchLocationComponent } from './components/search-location/search-location.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { LocationDetailsComponent } from './components/location-details/location-details.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbModule,
    RouterModule,
    LeafletModule,
  ],
  declarations: [
    ConfirmationComponent,
    LoginComponent,
    SpinnerComponent,
    NavigationComponent,
    SearchLocationComponent,
    MapViewLocationComponent,
    LocationDetailsComponent
  ],
  exports: [
    RouterModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    ConfirmationComponent,
    SpinnerComponent,
    SearchLocationComponent,
    NgbModule,
    FormsModule,
    NavigationComponent,
    LeafletModule,
    LocationDetailsComponent,
  ],
})
export class SharedModule { }
