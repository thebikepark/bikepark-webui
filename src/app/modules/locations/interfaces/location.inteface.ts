import { User } from '../../users/interfaces/user.interface';


export enum LocaitonType {
  Parking = "Parking",
  Theft = "Theft"
}

export interface Location {
  name: string
  city: string
  street: string
  description: string
  isSuggestion: boolean
  latitude: number
  longitude: number
  user: User
  type: LocaitonType
}
