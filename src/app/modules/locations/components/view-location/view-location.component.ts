import { Component, Input, OnInit } from '@angular/core';
import { icon, latLng, LatLng, Map, MapOptions, Marker, marker, MarkerOptions, tileLayer } from 'leaflet';
import { Location } from '../../interfaces/location.inteface';

@Component({
  selector: 'app-view-location',
  templateUrl: './view-location.component.html',
  styleUrls: ['./view-location.component.scss']
})
export class ViewLocationComponent implements OnInit {

  options: any;
  map: Map;
  marker: Marker;

  @Input() location: Location;

  constructor() { }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12
    }
  }

  onMapReady(e: Map): void {
    this.map = e;
    const latLngE = latLng(this.location.latitude, this.location.longitude)
    this.marker = this.setMarker(latLngE)
    this.marker.addTo(this.map)
    setTimeout(() => {
      this.map.setView(latLngE, 16)
    }, 500)
  }

  private setMarker(latLng: LatLng): Marker {
    const m = this.createMarkerFromLocation(latLng)
    return m
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    const options: MarkerOptions = {
      draggable: false,
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: 'leaflet/marker-icon.png',
        shadowUrl: 'leaflet/marker-shadow.png',
      })
    }
    return marker(latLng, options)
  }

}
