import { Component, EventEmitter, Input, NgZone, OnChanges, OnInit, Output, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as L from 'leaflet';
import { icon, latLng, Map, marker, Marker, MarkerOptions, popup, tileLayer } from 'leaflet';
import { GestureHandling } from "leaflet-gesture-handling";
import { AuthService } from 'src/app/shared/services/auth.service';
import { LocationDetailsComponent } from '../../../../shared/components/location-details/location-details.component';
import { bikeParkingIconPath, bikeTheftIconPath, LeafletService } from '../../../../shared/services/leaflet.service';
import { LocaitonType, Location } from '../../interfaces/location.inteface';


@Component({
  selector: 'app-locations-map',
  templateUrl: './locations-map.component.html',
  styleUrls: ['./locations-map.component.scss']
})
export class LocationsMapComponent implements OnInit, OnChanges {

  @Input() locations: Location[];
  @Input() setView: [number, number];

  @Output() moveEnd = new EventEmitter<[number, number]>();

  map: Map;
  layers: any[] = []
  options: any
  layersControl: any

  constructor(
    private modalService: NgbModal,
    private authService: AuthService,
    private zone: NgZone,
  ) {
    L.Map.addInitHook("addHandler", "gestureHandling", GestureHandling);
  }

  ngOnInit(): void {
    this.authService.isLoggedIn().subscribe(v => {  
      this.options = {
        layers: [
          tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
        ],
        zoom: 12,
        center: latLng(45.8103223, 15.9795587),
        scrollWheelZoom: false,
        gestureHandling: true
      };

    })
  }

  onMapReady(map: Map): void {
    this.map = map;
  }

  leafletMapMoveEnd(_: L.LeafletEvent): void {
    this.moveEnd.emit([this.map.getCenter().lat, this.map.getCenter().lng]);
  }

  ngOnChanges(changes: { [key: string]: SimpleChange }) {
    if (changes["locations"]) {
      this.locations = changes["locations"].currentValue;
      this.layers = [];
      this.locations.forEach(l => {
        this.layers.push(this.createMarkerFromLocation(l))
      })
    }
    if (changes["setView"]) {
      if (this.map) {
        setTimeout(() => {
          this.map.setView(this.setView, 13, {});
        }, 0);
      }
    }
  }

  reviews(loc: Location): void {
    const m = this.modalService.open(LocationDetailsComponent, {
      size: 'lg',
      centered: true

    })
    m.componentInstance.location = loc;
    m.result.then(_ => {
      
    }, _ => { })    
  }

  private createMarkerFromLocation(loc: Location): Marker {
    var iconUrl = bikeParkingIconPath;
    if (loc.type == LocaitonType.Theft) {
      iconUrl = bikeTheftIconPath;
    }

    const options: MarkerOptions = {
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: iconUrl,
        shadowUrl: 'leaflet/marker-shadow.png',
      })
    }
    const p = popup()
    const content = ` <div class="popup-content">
                        <div class="mb-3">
                          <div class="font-weight-bold" (click)="test()">Street: </div><div>${loc.street}</div>
                        </div>
                        <div class="mb-3">
                          <div class="font-weight-bold">Description:</div> <div>${loc.description}</div>
                        </div>
                      </div>
                    `
    p.setContent(content)
    return marker([loc.latitude, loc.longitude], options).on("click", () => {
      this.zone.run(() => {
        this.reviews(loc)
      })
    });
  }

}
