import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiServiceService } from '../../../shared/services/api-service.service';
import { User, UserLocation, UsersRespose } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends ApiServiceService<User, UsersRespose> {

  constructor(
    public http: HttpClient
  ) {
    super(http);
    this.url = "users";
  }

  // /api/v1/{name=users/*}:editLocation
  editLocation(name: string, location: UserLocation): Observable<UserLocation> {
    return this.http.put<UserLocation>(`/api/${this.getVersion()}/${name}:editLocation`, location);
  }

}
