import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBikeTheftComponent } from './edit-bike-theft.component';

describe('EditBikeTheftComponent', () => {
  let component: EditBikeTheftComponent;
  let fixture: ComponentFixture<EditBikeTheftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBikeTheftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBikeTheftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
