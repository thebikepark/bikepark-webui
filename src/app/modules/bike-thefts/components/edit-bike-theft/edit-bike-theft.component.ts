import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LatLng, latLng, Map, MapOptions, marker, Marker, MarkerOptions, tileLayer } from 'leaflet';
import { GeoLocation } from '../../../../shared/interfaces/geo-location.interface';
import { GeoLocationService } from '../../../../shared/services/geo-location.service';
import { bikeTheftIconPath, LeafletService } from '../../../../shared/services/leaflet.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeTheft } from '../../interfaces/bike-theft.interface';
import { BikeTheftsService } from '../../services/bike-thefts.service';

@Component({
  selector: 'app-edit-bike-theft',
  templateUrl: './edit-bike-theft.component.html',
  styleUrls: ['./edit-bike-theft.component.scss']
})
export class EditBikeTheftComponent implements OnInit {

  modalRef: NgbModalRef
  bikeTheftForm: FormGroup
  options: any
  map: Map
  lastMarker: Marker
  marker: Marker

  @Input() bikeTheft: BikeTheft

  constructor(
    public activeModal: NgbActiveModal,
    private geoSearchService: GeoLocationService,
    private bikeTheftsService: BikeTheftsService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService,
  ) {
    this.bikeTheftForm = new FormGroup({
      city: new FormControl(),
      street: new FormControl(),
      latitude: new FormControl({ value: '', disabled: true }),
      longitude: new FormControl({ value: '', disabled: true }),
      isFound: new FormControl(),
      description: new FormControl(),
    })
  }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12,
      scrollWheelZoom: false,
      center: latLng(45.8103223, 15.9795587)
    }

    this.bikeTheftForm.patchValue(<BikeTheft>{
      city: this.bikeTheft.city,
      street: this.bikeTheft.street,
      latitude: this.bikeTheft.latitude,
      longitude: this.bikeTheft.longitude,
      description: this.bikeTheft.description,
      isFound: this.bikeTheft.isFound,
    })
  }

  onMapReady(map: Map) {
    this.map = map
    const latLngE = latLng(this.bikeTheft.latitude, this.bikeTheft.longitude)
    this.marker = this.setMarker(latLngE)
    this.marker.addTo(this.map)
    setTimeout(() => {
      this.map.setView(latLngE, 16)
    }, 500)
  }

  onSelected(gl: GeoLocation) {
    if (gl) {
      if (this.lastMarker) {
        this.lastMarker.remove()
      }
      const foundLocation = gl;
      this.bikeTheftForm.patchValue({
        "city": foundLocation.city,
        "street": foundLocation.address,
        "latitude": foundLocation.latitude,
        "longitude": foundLocation.longitude,
      })
      const latLngE = latLng(foundLocation.latitude, foundLocation.longitude)
      this.map.setView(latLngE, 16)
      const marker = this.createMarkerFromLocation(latLngE)
      marker.on('dragend', e => {
        this.spinnerService.spin();
        const target = e.target
        const lat = target.getLatLng().lat
        const lng = target.getLatLng().lng
        this.geoSearchService.reverse(lat, lng).subscribe(
          r => {
            this.bikeTheftForm.patchValue({
              "city": r.city,
              "street": r.address,
              "latitude": r.latitude,
              "longitude": r.longitude,
            })
            this.spinnerService.stop();
          },
          _ => {
            this.spinnerService.stop();
            this.notificationsService.error(this.geoSearchService.errorMsg);
          }
        )
      })
      marker.addTo(this.map)
      this.lastMarker = marker
    }
  }


  search(value: string) {
    this.spinnerService.spin()
    if (value.trim()) {
      if (this.lastMarker) {
        this.lastMarker.remove()
      }
      this.geoSearchService.search(value).subscribe(
        r => {
          if (r.locations.length > 0) {
            const foundLocation = r.locations[0]
            this.bikeTheftForm.patchValue({
              "city": foundLocation.city,
              "street": foundLocation.address,
              "latitude": foundLocation.latitude,
              "longitude": foundLocation.longitude,
            })
            const latLngE = latLng(foundLocation.latitude, foundLocation.longitude)
            this.map.setView(latLngE, 16)
            const marker = this.createMarkerFromLocation(latLngE)
            marker.on('dragend', e => {
              this.spinnerService.spin();
              const target = e.target
              const lat = target.getLatLng().lat
              const lng = target.getLatLng().lng
              this.geoSearchService.reverse(lat, lng).subscribe(
                r => {
                  this.bikeTheftForm.patchValue({
                    "city": r.city,
                    "street": r.address,
                    "latitude": r.latitude,
                    "longitude": r.longitude,
                  })
                  this.spinnerService.stop();
                },
                (error: HttpErrorResponse) => {
                  this.notificationsService.fromError(error);
                }
              )
            })
            marker.addTo(this.map)
            this.lastMarker = marker
          }
          this.spinnerService.stop();
        },
        _ => {
          this.notificationsService.error(this.geoSearchService.errorMsg)
        }
      )
    }
  }

  close(loc?: BikeTheft): void {
    this.activeModal.close(loc)
  }

  save() {
    let loc: BikeTheft = {
      ...this.bikeTheftForm.getRawValue()
    }
    this.spinnerService.spin();
    this.bikeTheftsService.put(this.bikeTheft.name, loc).subscribe(
      resp => {
        this.notificationsService.success("Bike Theft successfully updated!")
        this.close(resp)
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.close();
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

  private setMarker(latLng: LatLng): Marker {
    const m = this.createMarkerFromLocation(latLng)
    m.on('dragend', e => {
      this.spinnerService.spin();
      const target = e.target;
      const lat = target.getLatLng().lat;
      const lng = target.getLatLng().lng;
      this.geoSearchService.reverse(lat, lng).subscribe(
        r => {
          this.bikeTheftForm.patchValue({
            "city": r.city,
            "street": r.address,
            "latitude": r.latitude,
            "longitude": r.longitude,
          })
          this.spinnerService.stop();
        },
        (error: HttpErrorResponse) => {
          this.spinnerService.stop();
          this.notificationsService.fromError(error);
        }
      )
    })
    return m
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    const options: MarkerOptions = {
      draggable: true,
      icon: LeafletService.icon(bikeTheftIconPath)
    }
    return marker(latLng, options)
  }
}
