import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LatLng, latLng, Map, MapOptions, marker, Marker, MarkerOptions, tileLayer } from 'leaflet';
import { GeoLocation } from '../../../../shared/interfaces/geo-location.interface';
import { GeoLocationService } from '../../../../shared/services/geo-location.service';
import { bikeTheftIconPath, LeafletService } from '../../../../shared/services/leaflet.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeTheft } from '../../interfaces/bike-theft.interface';
import { BikeTheftsService } from '../../services/bike-thefts.service';

@Component({
  selector: 'app-create-bike-theft',
  templateUrl: './create-bike-theft.component.html',
  styleUrls: ['./create-bike-theft.component.scss']
})
export class CreateBikeTheftComponent implements OnInit {

  modalRef: NgbModalRef;
  bikeTheftForm: FormGroup;
  options: any;
  lastMarker: Marker
  map: Map;

  constructor(
    public activeModal: NgbActiveModal,
    private geoSearchService: GeoLocationService,
    private bikeTheftsService: BikeTheftsService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService,
  ) {
    this.bikeTheftForm = new FormGroup({
      city: new FormControl(null, Validators.required),
      street: new FormControl(null, Validators.required),
      latitude: new FormControl(null, Validators.required),
      longitude: new FormControl(null, Validators.required),
      description: new FormControl(),
    })
  }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12,
      scrollWheelZoom: false,
      center: latLng(45.8103223, 15.9795587)
    }
  }

  onMapReady(map: Map) {
    this.map = map
  }

  onSelected(gl: GeoLocation) {
    if (gl) {
      if (this.lastMarker) {
        this.lastMarker.remove()
      }
      const foundLocation = gl;
      this.bikeTheftForm.patchValue({
        "city": foundLocation.city,
        "street": foundLocation.address,
        "latitude": foundLocation.latitude,
        "longitude": foundLocation.longitude,
      })

      const latLngE = latLng(foundLocation.latitude, foundLocation.longitude)
      this.map.setView(latLngE, 16)
      const marker = this.createMarkerFromLocation(latLngE)
      marker.on('dragend', e => {
        this.spinnerService.spin();
        const target = e.target
        const lat = target.getLatLng().lat
        const lng = target.getLatLng().lng
        this.geoSearchService.reverse(lat, lng).subscribe(
          r => {
            this.bikeTheftForm.patchValue({
              "city": r.city,
              "street": r.address,
              "latitude": r.latitude,
              "longitude": r.longitude,
            })
            this.spinnerService.stop();
          },
          e => {
            console.error("OpenStreetMap: ", e);
            this.notificationsService.error(this.geoSearchService.errorMsg);
          }
        )
      })
      marker.addTo(this.map)
      this.lastMarker = marker
    }
  }

  close(bt?: BikeTheft): void {
    this.activeModal.close(bt)
  }

  save() {
    let bt = {
      ...this.bikeTheftForm.getRawValue()
    };
    this.spinnerService.spin();
    this.bikeTheftsService.post(bt).subscribe(
      (_) => {
        this.close(bt)
        this.notificationsService.success("Bike Theft successfully created.");
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.notificationsService.fromError(error);
        this.spinnerService.stop();
      }
    )
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    const options: MarkerOptions = {
      draggable: true,
      icon: LeafletService.icon(bikeTheftIconPath)
    }
    return marker(latLng, options)
  }

}
