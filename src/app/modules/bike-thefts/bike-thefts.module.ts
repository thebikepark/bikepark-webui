import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LeafletDirective, LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from '../../shared/shared.module';
import { BikeTheftsRoutingModule } from './bike-thefts-routing.module';
import { CreateBikeTheftComponent } from './components/create-bike-theft/create-bike-theft.component';
import { EditBikeTheftComponent } from './components/edit-bike-theft/edit-bike-theft.component';
import { MyBikeTheftsComponent } from './components/my-bike-thefts/my-bike-thefts.component';


@NgModule({
  declarations: [
    MyBikeTheftsComponent,
    CreateBikeTheftComponent,
    EditBikeTheftComponent
  ],
  providers: [
    LeafletDirective
  ],
  imports: [
    BikeTheftsRoutingModule,
    SharedModule,
    CommonModule,
    LeafletModule
  ]
})
export class BikeTheftsModule { }
