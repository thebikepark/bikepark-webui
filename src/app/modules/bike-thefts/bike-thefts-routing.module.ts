import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyBikeTheftsComponent } from './components/my-bike-thefts/my-bike-thefts.component';


const routes: Routes = [
  {
    path: 'my-bikethefts',
    component: MyBikeTheftsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeTheftsRoutingModule { }
