import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from '../../../app/shared/shared.module';
import { BikeParkRoutingModule } from './bike-park-routing.module';
import { CreateBikeParkComponent } from './components/create-bike-park/create-bike-park.component';
import { EditBikeParkComponent } from './components/edit-bike-park/edit-bike-park.component';
import { MyBikeParkingsComponent } from './components/my-bike-parkings/my-bike-parkings.component';



@NgModule({
  declarations: [
    EditBikeParkComponent,
    CreateBikeParkComponent,
    MyBikeParkingsComponent
  ],
  imports: [
    SharedModule,
    LeafletModule,
    BikeParkRoutingModule
  ]
})
export class BikeParkModule { }
