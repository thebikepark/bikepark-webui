import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyBikeParkingsComponent } from './my-bike-parkings.component';

describe('MyBikeParkingsComponent', () => {
  let component: MyBikeParkingsComponent;
  let fixture: ComponentFixture<MyBikeParkingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyBikeParkingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyBikeParkingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
