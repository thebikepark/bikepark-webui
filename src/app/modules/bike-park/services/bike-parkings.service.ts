import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiServiceService } from '../../../shared/services/api-service.service';
import { BikeParking, BikeParkingsResponse } from '../interfaces/bike-parking.interface';

export interface NearstQuery {
  latitude: number
  longitude: number
  limit: number,
  distance: number
}

@Injectable({
  providedIn: 'root'
})
export class BikeParkingsService extends ApiServiceService<BikeParking, BikeParkingsResponse> {

  createdMessage = "Bike park created successfully"
  editMessage = "Bike park updated successfully"

  constructor(public http: HttpClient) {
    super(http)
    this.url = "bikeparkings";
  }

  nearest(query: NearstQuery): Observable<BikeParkingsResponse> {
    const p = new HttpParams({
      fromObject: {
        "filter.latitude": query.latitude.toString(),
        "filter.longitude": query.longitude.toString(),
        "filter.distance": query.distance.toString(),
        "limit": query.limit.toString()
      }
    })
    return this.http.get<BikeParkingsResponse>(`${this.getFullUrl()}:nearest`, { params: p });
  }

  myBikeParkings(): Observable<BikeParkingsResponse> {
    return this.http.get<BikeParkingsResponse>(`${this.getFullUrl()}:myBikeParkings`);
  }

}
