DOCKERNET:=bikepark
PRODUCT="thebikepark/bikepark-webui"
IMAGETAG=$(PRODUCT):latest
CI_REGISTRY=registry.gitlab.com

.PHONY: docker-network
docker-network: ## spin up the local bikepark docker network so that all dockerized gym components can communicate
	if [ -z $$(docker network ls -q --filter 'name=$(DOCKERNET)') ]; then\
		docker network create $(DOCKERNET);\
	fi

.PHONY: docker-image-build
docker-image-build:
	docker build \
		-t $(CI_REGISTRY)/$(IMAGETAG) \
    -f build/docker/Dockerfile .

.PHONY: docker-up
docker-up: docker-network
	docker-compose --project-directory . \
	-f deployments/docker/docker-compose.yml \
	up \
	-d \
	--force-recreate \
	--build \
	--remove-orphans

.PHONY: docker-logs
docker-logs:
	docker-compose  --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	logs

.PHONY: generate-ssh
generate-ssh:
	bash ./scripts/generatessh.sh
